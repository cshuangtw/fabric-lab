#!/bin/bash

# 定義要發送的交易總數和會話數
total_transactions=100
sessions=10
transactions_per_session=$((total_transactions / sessions))

# 函數來發送交易
send_transactions() {
  for ((i=1; i<=transactions_per_session; i++)); do
    # 隨機生成資產名稱和其他參數
    asset_name="asset_$RANDOM"
    color="purple"
    size="$RANDOM"
    owner="Alexandra"
    appraisedValue="$RANDOM"

    # 調用 chaincode 函數
    peer chaincode invoke -C channel1 -n basic -c "{\"function\":\"CreateAsset\",\"Args\":[\"$asset_name\",\"$color\",\"$size\",\"$owner\",\"$appraisedValue\"]}" -o orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --peerAddresses peer0.org1.com:1051 --tlsRootCertFiles $PWD/../tlsca/tlsca.org1.com-cert.pem --peerAddresses peer0.org2.com:2051 --tlsRootCertFiles $PWD/../tlsca/tlsca.org2.com-cert.pem

    # 簡單的延遲以避免過度壓力
    sleep 1
  done
}

# 在背景中執行指定數量的會話
for ((session=1; session<=sessions; session++)); do
  send_transactions &
done

# 等待所有後台任務完成
wait
echo "所有交易已發送完成。"

