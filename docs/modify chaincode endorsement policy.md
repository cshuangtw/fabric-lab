# Modify chaincode endorsement policy 

## 情境描述

1. 現有環境:

    1. peerOrganizations: Org1MSP, Org2MSP
    2. 部署 channel: channel1
    3. 部署 chaincode: asset-transfer-basic, 
       - label: basic_1.0
       - sequence: 1
       - 部署節點: peer0.org1.com, peer0.org2.com
    4. endorsement policy: MAJORITY

2. 要將 chaincode basic 的 endorsement policy 修改為 OR ('Org1MSP.peer','Org2MSP.peer'), 即角色 Org1MSP.peer 或者 Org2MSP.peer endorse 即可。
3. 差異: 原 MAJORITY policy 必須過半數的機構背書才能滿足背書策略，後者則為 Org1MSP 及 Org2MSP 任一機構的背書節點簽章即可滿足背書策略.

## 實作

1. 查詢 已安裝的 chaincode CC_ID (chaincode ID)

***`[command]`***
```bash
cd $HOME/workspaces/fabric-lab/workdir/org1-client
source admin.env peer0 1051
cd $HOME/workspaces/fabric-lab/workdir/org1-client/tmp
peer lifecycle chaincode queryinstalled --output json
```
`System Response:`
```json
{
        "installed_chaincodes": [
                {
                        "package_id": "basic_1.0:1f4749caf72871f38e43a65861a31f1708c541d306143287ecbfe068fa7dd5bb",
                        "label": "basic_1.0",
                        "references": {
                                "channel1": {
                                        "chaincodes": [
                                                {
                                                        "name": "basic",
                                                        "version": "1.0"
                                                }
                                        ]
                                }
                        }
                }
        ]
}
```
***`[command]`***
```bash
export CC_PACKAGE_ID=$(peer lifecycle chaincode queryinstalled --output json |jq -r '.installed_chaincodes[]|select(.label=="basic_1.0
").package_id')
```
`System Response:`
```json
basic_1.0:1f4749caf72871f38e43a65861a31f1708c541d306143287ecbfe068fa7dd5bb
```

2. 重新 approve chaincode, 要將 sequence 加 1

***`[command]`***
```bash
peer lifecycle chaincode approveformyorg --channelID channel1 --name basic --version 1.0 --sequence 2 --orderer orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --package-id $CC_PACKAGE_ID --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer')"
```
`System Response:`
```log
2023-11-28 21:38:32.017 UTC [chaincodeCmd] ClientWait -> INFO 001 txid [0b5db49d3bcade29f0e3ce607ac641489fd7a7f7d3ca0e54bbdaa6dcf95152d3] committed with status (VALID) at peer0.org1.com:1051
```

3. 檢查提交準備狀況 (check commit readiness)

***`[command]`***
```bash
peer lifecycle chaincode checkcommitreadiness --channelID channel1 --name basic --version 1.0 --sequence 2 --orderer orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer')"
```

`System Response:`

```bash
Chaincode definition for chaincode 'basic', version '1.0', sequence '2' on channel 'channel1' approval status by org:
Org1MSP: true
Org2MSP: false
```

4. 切換角色, 由 Org2MSP admin 執行 approve 新的 endorsement policy, 檢查 chaincode id

***`[command]`***
```bash
cd $HOME/workspaces/fabric-lab/workdir/org2-client
source admin.env peer0 2051
cd $HOME/workspaces/fabric-lab/workdir/org2-client/tmp
export CC_PACKAGE_ID=$(peer lifecycle chaincode queryinstalled --output json|jq -r '.installed_chaincodes[]|select(.label=="basic_1.0").package_id')
echo $CC_PACKAGE_ID
```
`System reponse:`

```bash
basic_1.0:1f4749caf72871f38e43a65861a31f1708c541d306143287ecbfe068fa7dd5bb
```
5. 重新 approve chaincode, 要將 sequence 加 1

***`[command]`***

```bash
peer lifecycle chaincode approveformyorg --channelID channel1 --name basic --version 1.0 --sequence 2 --orderer orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --package-id $CC_PACKAGE_ID --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer')"
```
`System Response:`

```bash
2023-11-28 21:59:00.525 UTC [chaincodeCmd] ClientWait -> INFO 001 txid [f72a8100475af09a397e7bab6aeca4d9c2473a8dc0f891659b83c5b57b06348a] committed with status (VALID) at peer0.org2.com:2051
```

6. 檢查提交準備狀況 (check commit readiness)

***`[command]`***

```bash
peer lifecycle chaincode checkcommitreadiness --channelID channel1 --name basic --version 1.0 --sequence 2 --orderer orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer')" 
```
`System Response:`

```bash
Chaincode definition for chaincode 'basic', version '1.0', sequence '2' on channel 'channel1' approval status by org:
Org1MSP: true
Org2MSP: true
```

7. 提交

***`[command]`***
```bash
peer lifecycle chaincode commit --channelID channel1 --name basic --version 1.0 --sequence 2 --orderer orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer')" --peerAddresses peer0.org1.com:1051 --tlsRootCertFiles $PWD/../tlsca/tlsca.org1.com-cert.pem --peerAddresses peer0.org2.com:2051 --tlsRootCertFiles $PWD/../tlsca/tlsca.org2.com-cert.pem
```

`System Response:`

```bash
2023-11-28 22:06:55.051 UTC [chaincodeCmd] ClientWait -> INFO 001 txid [75102bde64191aa997142c4ede702edad74f52664dbb2f2c8be18797df56ce2b] committed with status (VALID) at peer0.org1.com:1051
2023-11-28 22:06:55.054 UTC [chaincodeCmd] ClientWait -> INFO 002 txid [75102bde64191aa997142c4ede702edad74f52664dbb2f2c8be18797df56ce2b] committed with status (VALID) at peer0.org2.com:2051
```

```
peer lifecycle chaincode querycommitted --channelID channel1 -n basic --output json
```
```
{
        "sequence": 2,
        "version": "1.0",
        "endorsement_plugin": "escc",
        "validation_plugin": "vscc",
        "validation_parameter": "CiwSDBIKCAESAggAEgIIARoNEgsKB09yZzFNU1AQAxoNEgsKB09yZzJNU1AQAw==",
        "collections": {},
        "approvals": {
                "Org1MSP": true,
                "Org2MSP": true
        }
```

8. 由 OrgMSP.client 測試提交一筆交易

   1. 查詢現有交易資料
   
   ***`[command]`***
   ```bash
   peer chaincode query -C channel1 -n basic -c '{"function":"GetAllAssets","Args":[]}'|jq
   ```
   `System Response:`
   ```bash
   [
     {
       "ID": "asset1",
       "color": "blue",
       "size": 5,
       "owner": "Tomoko",
       "appraisedValue": 300
     },
     {
       "ID": "asset2",
       "color": "red",
       "size": 5,
       "owner": "Brad",
       "appraisedValue": 400
     },
     {
       "ID": "asset3",
       "color": "green",
       "size": 10,
       "owner": "Jin Soo",
       "appraisedValue": 500
     },
     {
       "ID": "asset4",
       "color": "yellow",
       "size": 10,
       "owner": "Max",
       "appraisedValue": 600
     },
     {
       "ID": "asset5",
       "color": "black",
       "size": 15,
       "owner": "Adriana",
       "appraisedValue": 700
     },
     {
       "ID": "asset6",
       "color": "white",
       "size": 15,
       "owner": "Michel",
       "appraisedValue": 800
     }
   ]   
   ```

   2. 刪除 asset5

   ***`[command]`***

   ```bash
   peer chaincode invoke -C channel1 -n basic -c '{"function":"DeleteAsset","Args":["asset5"]}' -o orderer0.org4.com:4050 --tls --cafile $ORDERER_TLS_CA --peerAddresses peer0.org2.com:2051 --tlsRootCertFiles $PWD/../tlsca/tlsca.org2.com-cert.pem
   ```
   `System Response:`
   ```bash
   2023-11-28 22:21:17.332 UTC [chaincodeCmd] chaincodeInvokeOrQuery -> INFO 001 Chaincode invoke successful. result: status:200 
   ```
   3. 再執行 8.1 GetAllAssets 查詢 驗證 asset5 是否已刪除
   
    
