# Upgrade chaincode

## 說明

任何一執行程序都會有優化或修正的需求，在 chaincode 源碼被改變後，必須 upgrade chaincode。

Upgrade chaincode 必須修改 version 以及 sequence 必須再加 1. 

## 實作說明

1. 假想以 asset-transfer-abac chaincode 為  asset-transfer-basic 的更新版。 二者的 Asset struct 都相同，差異在於 asset-transfer-basic 僅檢查提交者是否在預設的權限 (configtx.yaml Application section 中定義 有 write 權限者 )，並沒有任何特殊限制提交者的身份。所以任一 organizations 具有 writer 權限者 (admin, client) 皆可以更改資產資料，Update, Transfer, Delete, Create. 而實務情境並不然。
2. asset-transfer-abac 是一以 每個參與個體 (client, admin) 的 MSP certificate 中的 attribution 中定義的屬性決定其對特定資產的操作權限。


